require("@tensorflow/tfjs-node");
const expressValidator = require("express-validator");
import express from "express"
import cors from "cors";
import https from "https";
import "dotenv/config";
import bodyParser from "body-parser";
import fs from "fs";
import { load } from "./faceRecognition";
import { output as models } from "./models";
import { PORT, DOMAIN } from "./constants";
import user from "./user/routes";
const httpsOptions = {
    key: fs.readFileSync(process.env.SSL_KEY_DIR),
    cert: fs.readFileSync( process.env.SSL_CERT_DIR)
};

// Setup express
const app = express();
app.set("DOMAIN", DOMAIN)
app.set("port", PORT);

// Setup models
models.setup({
	dbUrl: process.env.MONGODB_URL,
	user: process.env.MONGODB_USER,
	password: process.env.MONGODB_PASSWORD
});

// Redirect https
app.enable("trust proxy");
app.use (function (req, res, next) {
	if (req.secure) {
		next();
	} else {
		res.redirect("https://" + req.headers.host + req.url);
	}
});

app.use(cors({
  origin: "https://"+DOMAIN
}));
app.use(express.static("public"))
app.use(express.json());
app.use(bodyParser.json({limit: "10MB"}));
app.use(expressValidator());


app.get("/", (req, res) => {
  return res.send("Received a GET HTTP method");
});

app.post("/api/login", user.login);
app.post("/api/register", user.register);

// Create server
https.createServer(httpsOptions, app).listen(app.get("port"), load);
