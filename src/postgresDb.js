const { Client } = require("pg");
let client;

module.exports = {
	runQuery
}

// Setup client
async function setup({ssl, user}){
	// Create client
	const conf = {};
	if(ssl){ conf.ssl = ssl; }
	if(user){ conf.user = user; }
	client = new Client(conf);

	// connect client
	try{
		await client.connect();
	}catch(error){
		console.error("Postgres DB Setup error: ", error);
	}
}

async function runQuery(query){
	// config client
	await setup({
	  ssl: { rejectUnauthorized: false }, // allow self-signed cert
	});

	let res;
	try{
		res = await client.query(query);
	}catch(error){
		console.error("Error executing Postgres query: ", error);
	}

	// shutdown client
	await shutdown();

	// send query result
	return res;
}

async function shutdown()
{
	await client.end();
}
