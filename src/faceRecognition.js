import canvas from 'canvas'
import * as faceapi from 'face-api.js'
import fs from 'fs'
import { monkeyPatchFaceApiEnv } from './monkeyPatch'
import path from 'path';
import {LABELED_IMAGES_URL, MODELS_URL, CAPTURED_IMG_URL, PORT} from './constants'

monkeyPatchFaceApiEnv();

export const load = () => Promise.all([
    faceapi.nets.faceRecognitionNet.loadFromDisk(MODELS_URL),
    faceapi.nets.faceLandmark68Net.loadFromDisk(MODELS_URL),
    faceapi.nets.ssdMobilenetv1.loadFromDisk(MODELS_URL),
    faceapi.nets.ageGenderNet.loadFromDisk(MODELS_URL),
    faceapi.nets.faceExpressionNet.loadFromDisk(MODELS_URL)
]).then(start);

const start = async () => {
    console.log(`Server listening on port ${PORT}`);
}

export const identifyFace = async ({userid, newImage, storedImages}) => {
    // load images
    newImage = await canvas.loadImage(newImage);
    const storedImg1 = await canvas.loadImage(storedImages[0]);
    const storedImg2 = await canvas.loadImage(storedImages[1]);
    storedImages = [newImage, newImage];

    // Get face
    const displaySize = { width: newImage.width, height: newImage.height };
    const detections = await faceapi.detectSingleFace(newImage).withFaceLandmarks().withFaceExpressions().withAgeAndGender().withFaceDescriptor();

    // Return if any detections are found
    if(!detections){ return; }
    const resizedDetections = faceapi.resizeResults(detections, displaySize);

    // get faceMatcher
    const faceMatcher = await getFaceMather({storedImages: storedImages, label: userid})
    const results = faceMatcher.findBestMatch(resizedDetections.descriptor);

    // Send results
    return {...results, ...resizedDetections};
}

async function getFaceMather({storedImages, label}){
    const descriptions = []
    for(let img of storedImages){
        const detections = await faceapi.detectSingleFace(img).withFaceLandmarks().withFaceDescriptor();
        descriptions.push(detections.descriptor);
    }
    const labeledFaceDescriptors = new faceapi.LabeledFaceDescriptors(label, descriptions);
    return new faceapi.FaceMatcher(labeledFaceDescriptors, 0.6);
}