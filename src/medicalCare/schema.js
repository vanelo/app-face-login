import { Schema } from "mongoose";

export { medicalCareSchema };

// Create schema
const medicalCareSchema = new Schema({
	id: { type: String },
	service: {
		id: { type: String },
		name: { type: String }
	},
	startingDate: { type: Date },
	endingDate: { type: Date },
	onlineMeeting: {
		url: { type: String },
		user: { type: String },
		password: { type: String }
	},
	status: { type: String }, //approved|pending|rejected|canceled
	provider: {
		id: { type: Number },
		name: { type: String },
		lastName: { type: String },
		email: { type: String }
	},
	customer: {
		id: { type: Number },
		idCardNumber: { type: Number },
		name: { type: String },
		lastName: { type: String },
		email: { type: String },
		phone: { type: String }
	},
	location: {
		id: { type: Number },
		name: { type: String },
	},
	scheduled: { type: Boolean, default: false }
});
