
import bodyParser from "body-parser";
import { load } from "./faceRecognition";
import { PORT, DOMAIN } from './constants'
import user from "./user/routes";
import "dotenv/config"; // Only for development, then delete .env file after set env variables
require('@tensorflow/tfjs-node');
const expressValidator = require("express-validator");
var express = require('express'),
    // routes = require('./routes'),
    // upload = require('./routes/upload'),
    http = require('http'),
    https = require('https'),
    fs = require('fs'),
    path = require('path'),
    httpApp = express(),
    app = express(),
    certPath = "cert";


var httpsOptions = {
    key: fs.readFileSync(process.env.SSL_KEY_DIR),
    cert: fs.readFileSync( process.env.SSL_CERT_DIR)
};
httpApp.set('port', 5000);
httpApp.get("*", function (req, res, next) {
    res.redirect("https://" + req.headers.host + "/" + req.path);
});

// all environments
app.set('port', 8443);
// app.set('views', __dirname + '/views');
// app.set('view engine', 'jade');
app.enable('trust proxy');
// app.use(express.favicon());
// app.use(express.logger('dev'));
// app.use(express.bodyParser());
// app.use(express.methodOverride());
// app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.json({limit: "10MB"}));
app.use(expressValidator());

// development only
// if('development' == app.get('env')) {
//     app.use(express.errorHandler());
// }

app.post('/login', user.login);
app.post('/register', user.register);


http.createServer(httpApp).listen(httpApp.get('port'), function() {
    console.log('Express HTTP server listening on port ' + httpApp.get('port'));
});

https.createServer(httpsOptions, app).listen(app.get('port'), function() {
    console.log('Express HTTPS server listening on port ' + app.get('port'));
});