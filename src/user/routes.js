import * as postgresDb from "../postgresDb.js";
import * as jwt from "jsonwebtoken";
import moment from "moment";
import {output as models} from "../models";
import { identifyFace } from "../faceRecognition";
import {LOGIN_REDIRECT_URL} from "../constants";

module.exports = {
	register,
	login
}

async function register(req, res){
	// Validate
	req.checkBody("image1").isBase64();
	req.checkBody("image2").isBase64();
	req.checkBody("usercardnumber").isLength({min:1, max:300});
	const validationResult = await req.getValidationResult(req);
	if(!validationResult.isEmpty()){res.status(400).send({type: validationResult.array()[0].param.toUpperCase+"_INVALID"}); return;}

	// Verify if new register is enable
	let registerIsPosible = true;
	const userid = req.body.usercardnumber.toLowerCase();
	const userExistRes = await postgresDb.runQuery(`SELECT * FROM ad_user_face WHERE userid = '${userid}'`);
	if(userExistRes.rows.length > 0){
		const userUpdateableRes = await postgresDb.runQuery(`SELECT * FROM ad_user_face WHERE userid = '${userid}' AND isupdateable = 'Y'`);
		if(userUpdateableRes.rows.length === 0){ registerIsPosible = false; }
	}

	// Save in bd
	if(registerIsPosible){
		// save the token in the user account
		await postgresDb.runQuery(`UPDATE ad_user_face SET isupdateable = 'N', isactive = 'N' WHERE userid = '${userid}'`);

		await postgresDb.runQuery(`
			INSERT
			INTO ad_user_face(isactive, userid, face_image1, face_image2, isenable, isupdateable, default_url)
			VALUES('Y', '${userid}', '${req.body.image1}', '${req.body.image2}', 'Y', 'N', '${LOGIN_REDIRECT_URL}')`);

		// send response
		res.status(200).send({userid: req.body.userid});
	}else{
		// send response
		res.status(400).send({type: "CANNOT_REGISTER_A_USERFACE"});
	}
}

async function login(req, res){
	// Validate
	req.checkBody("image").isBase64();
	req.checkBody("usercardnumber").isLength({min:1, max:300});
	const validationResult = await req.getValidationResult(req);
	if(!validationResult.isEmpty()){res.status(400).send({type: validationResult.array()[0].param.toUpperCase+"_INVALID"}); return;}

	// Get user from db
	const userid = req.body.usercardnumber.toLowerCase();
	const userQueryRes = await postgresDb.runQuery(`SELECT userid, isactive, isenable, face_image1, face_image2 FROM ad_user_face WHERE userid = '${userid}'`);

	let user = userQueryRes.rows[0];
	for(let thisUser of userQueryRes.rows){
		if(thisUser.isactive === "Y"){
			user = thisUser;
		}
	}
	if(!user){res.status(400).send({type: "USER_NOT_FOUND"}); return;}
	if(user.isactive !== "Y"){res.status(400).send({type: "USER_INACTIVE"}); return;}
	if(!user.face_image1 || !user.face_image1){res.status(400).send({type: "FACE_IMG_NOT_FOUND"}); return;}

	// Convert base64 to buffer
	const newImage = Buffer.from(req.body.image, "base64");
	const storedImages = [Buffer.from(req.body.image, "base64"), Buffer.from(req.body.image, "base64")];

	// Verify user
	const result = await identifyFace({
		userid,
		newImage,
		storedImages
	});
	if(!result){res.status(400).send({type: "RESULTS_NOT_FOUND"}); return;}
	if(result._label !== userid){res.status(400).send({type: "USER_NOT_MATCHED"}); return;}

	// Get meeting info
	const dateFilterFrom = +moment(Date.now()).add(2, "h"); // now + 2hours
	const medicalCare = await models.MedicalCare.findOne({
		"customer.idCardNumber": parseInt(userid),
		status: "approved",
		startingDate: {$lte: dateFilterFrom}
	}).sort({startingDate:-1});
	if(!medicalCare || !medicalCare.onlineMeeting || !medicalCare.onlineMeeting.url){
		res.status(400).send({type: "NO_MEETING_FOUND"});
		return;
	}

	// Create a token
	const token = jwt.sign({
		userid: req.body.usercardnumber
	}, process.env.JWT_PRIVATE_KEY, {
		algorithm: "RS256"
	});

	// save the token in the user account
	await postgresDb.runQuery(`UPDATE ad_user_face SET token = '${token}' WHERE userid = '${userid}'`);

	// Send response
	return res.send({
		meetingUrl: medicalCare.onlineMeeting.url,
		meetingUser: medicalCare.onlineMeeting.user,
		meetingPassword: medicalCare.onlineMeeting.password,
		result,
		token
	});
}