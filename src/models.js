import mongoose from "mongoose";
import { medicalCareSchema } from "./medicalCare/schema";
var mongooseConnection;
const output = {
	setup,
	disconnect
};
export {
	output
};

async function setup(config){
	// Setup mongoose
	mongoose.set("useCreateIndex", true);
	mongoose.set("useFindAndModify", false);
	mongoose.set("useUnifiedTopology", true);
	if(mongooseConnection){ mongooseConnection.removeAllListeners(); }	// This is to ease testing
	while(!mongooseConnection){
		try {
			mongooseConnection = await mongoose.createConnection(config.dbUrl, {
				user: config.user,
				pass: config.password,
				useNewUrlParser: true,
				bufferMaxEntries: 0					// Throw error immediatly if not connected
			});
		} catch(error) {
			console.error(error);
			await new Promise(r => setTimeout(r, 1000));
			continue;
		}
	}
	mongooseConnection.on("error", (error) => {
		console.error("Mongoose connection error", error);
		throw error;
	});
	mongooseConnection.on("disconnected", () => log.warn("Disconnected from MongoDB"));

	// Setup models
	output.MedicalCare = mongooseConnection.model("MedicalCare", medicalCareSchema);

	// Create collections
	for(let model of Object.keys(output).filter(k => !["setup", "disconnect"].includes(k))){
		await output[model].createCollection().catch(error => {
			if(error.codeName != "NamespaceExists"){ throw error; }
		});
	}
}

async function disconnect()
{
	await mongooseConnection.close();
	mongooseConnection = null;
}
