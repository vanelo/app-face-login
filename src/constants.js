import path from "path";

// Models dir
export const MODELS_URL = path.join(__dirname, "/../models");

// Server info
export const PORT = process.env.PORT || 3000;
export const DOMAIN = process.env.DOMAIN || "localhost";
export const LOGIN_REDIRECT_URL = "google.com" // This is not used