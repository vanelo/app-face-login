# A web server that recognises you if you are registered in the system

## Technologies used
- <a href="https://github.com/justadudewhohacks/face-api.js/">Face Api Js</a> JavaScript API for face detection and face recognition in the browser and nodejs with tensorflow.js
- <a href="https://www.tensorflow.org/js">TensorFlow.js</a> Library for machine learning in JavaScript
- Express js to create a node server
- Jquery, HTML and CSS for web page
- Postgres db

## Running on local machine
- `npm install` To install dependencies
- `npm start` To run the server

## Default constants
You can change this constants used in the project:
### In public >login.js
- DOMAIN
- PORT
- LOGIN_REDIRECT_URL

### In src > constants.js
- MODELS_URL
- PORT
- DOMAIN

### Environment vars
- PORT
- NODE_TLS_REJECT_UNAUTHORIZED
- DOMAIN
- JWT_PRIVATE_KEY
- JWT_PUBLIC_KEY
- PGHOST
- PGUSER
- PGDATABASE
- PGPASSWORD
- PGPORT

### Nontes
- userid in postgres db is the car number of client, that is customer.idCardNumber in medicalCare schema in mongodb
- Only existing users in the db without facial image can register
## Credit
- <a href="https://github.com/spoman007/face-login-website">Based in this project</a>

### Author: Vanessa Lopez

