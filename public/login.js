// Server info
const DOMAIN = "facelogin.plantelmedico.com";
const PORT = 5000;

// Messages
const msg = {}
msg.ENTER_USERID = "Ingrese su nombre de usuario";
msg.LOOK_AT_CAMERA = "Por favor, mira a la cámara";
msg.LOOK_LEFT = "Por favor, mira a la izquierda";
msg.UNABLE_TO_ACCESS_CAMERA = "No se puede acceder a la cámara";
msg.UNSUPPORT_MEDIA_DEVICES = "Tu navegador no soporta dispositivos multimedia";
msg.USERID_INVALID = "Ingrese un usuario válido";
msg.IMAGE_INVALID = "Ingrese una imagen válida";
msg.IMAGE1_INVALID = "Ingrese una imagen válida";
msg.IMAGE2_INVALID = "Ingrese una imagen válida";
msg.USER_REGISTERED = "Usuario registrado";
msg.CANNOT_REGISTER_A_USERFACE = "No esta habilitado para registrar un usuario";
msg.USER_NOT_FOUND = "Usuario no encontrado";
msg.FACE_IMG_NOT_FOUND = "Registre su rostro con la cámara";
msg.USER_INACTIVE = "Usuario inactivo";
msg.RESULTS_NOT_FOUND = "No se pudo identiciar al usurio";
msg.USER_NOT_MATCHED = "Usuario no identificado";
msg.USER_MATCHED = "Usuario identificado";
msg.NO_MEETING_FOUND = "No tiene consultas pendientes";

// The buttons to start & stop stream and to capture the image
let btnStart = document.getElementById("btn-start");
let btnRegister = document.getElementById("btn-register");

//Media container
let mediaContainer = document.getElementById("media-window");

// The stream & capture
let stream = document.getElementById("stream");
let capture = document.getElementById("capture");
let snapshot = document.getElementById("snapshot");

// The video stream
let cameraStream = null;

function promisify(xhr, failNon2xx = true) {
	const oldSend = xhr.send;
	xhr.send = function () {
		const xhrArguments = arguments;
		return new Promise(function (resolve, reject) {
			xhr.onload = function () {
				if (failNon2xx && (xhr.status < 200 || xhr.status >= 300)) {
					if(xhr.status === 400){
						const response = JSON.parse(xhr.response);
						errorStyleHandle({errorCode: response.type});
					}
					reject({ request: xhr });
				} else {
					resolve(xhr);
				}
			};
			xhr.onerror = function () {
				reject({ request: xhr });
			};
			oldSend.apply(xhr, xhrArguments);
		});
	}
}

async function startStreamingRegister() {
	let mediaSupport = 'mediaDevices' in navigator;
	if (mediaSupport && null == cameraStream) {
		navigator.mediaDevices.getUserMedia({ video: true })
			.then(function (mediaStream) {
				mediaContainer.className = "media-window";

				cameraStream = mediaStream;

				stream.srcObject = mediaStream;

				stream.play();
				setTimeout(async() => {
					await captureSnapshotsRegister();
				}, 1000);
			})
			.catch(function (err) {
				// Display error
				errorStyleHandle({errorCode: "UNABLE_TO_ACCESS_CAMERA"});
			});
	}else if(!mediaSupport){
		// Display error
		errorStyleHandle({errorCode: "UNSUPPORT_MEDIA_DEVICES"});
		return;
	}

	// stop Streaming
	await stopStreaming();
}

async function captureSnapshotsRegister() {
	if (null != cameraStream) {
		let data = {};
		const usercardnumber = document.getElementById("usercardnumber").value.trim()
		if (usercardnumber.length === 0) {
			stopStreaming();
			// Display error
			errorStyleHandle({errorCode: "ENTER_USERID"});
			return;
		}
		data["usercardnumber"] = usercardnumber;

		for (i = 0; i < 2; i++) {
			let ctx = capture.getContext('2d');
			let img = new Image();

			ctx.drawImage(stream, 0, 0, capture.width, capture.height);

			img.src = capture.toDataURL("image/jpeg");
			img.width = 240;

			const res = await fetch(img.src)

			const base64img = res.url.split(";base64,").pop();

			data[`image${i+1}`] = base64img;
		}

		data = JSON.stringify(data);

		let xhr = new XMLHttpRequest();
		promisify(xhr);

		// open request
		xhr.open("POST", `https://${DOMAIN}:${PORT}/api/register`);

		// set `Content-Type` header
		xhr.setRequestHeader("Content-Type", "application/json");

		// send request with json payload
		xhr.send(data).then(res => {
			stopStreaming();
			// Display message
			alert(msg["USER_REGISTERED"]);
			clearStyle();
		}).catch(()=> stopStreaming());
	}
}

// Start Streaming
async function startStreaming() {
	let mediaSupport = 'mediaDevices' in navigator;
	if(mediaSupport && null == cameraStream) {
		navigator.mediaDevices.getUserMedia({ video: true })
			.then(function (mediaStream) {
				mediaContainer.className = "media-window";

				cameraStream = mediaStream;

				stream.srcObject = mediaStream;

				stream.play();
				setTimeout(async() => {
					await captureSnapshot();
				}, 2000);
			})
			.catch(function (err) {
				// Display error
				errorStyleHandle({errorCode: "UNABLE_TO_ACCESS_CAMERA"});
			});
	}else if(!mediaSupport){
		// Display error
		errorStyleHandle({errorCode: "UNSUPPORT_MEDIA_DEVICES"});
		return;
	}
}

// Stop Streaming
function stopStreaming() {
	mediaContainer.className = "hide-window";
	if (null != cameraStream) {
		let track = cameraStream.getTracks()[0];
		track.stop();
		stream.load();
		cameraStream = null;
	}
}

async function captureSnapshot() {
	// get usercardnumber
	const usercardnumber = document.getElementById("usercardnumber").value.trim();
	if (usercardnumber.length === 0) {
		await stopStreaming();

		// Display error
		errorStyleHandle({errorCode: "ENTER_USERID"});
		return;
	}

	if (null != cameraStream) {

		let ctx = capture.getContext('2d');
		let img = new Image();

		ctx.drawImage(stream, 0, 0, capture.width, capture.height);

		img.src = capture.toDataURL("image/jpeg");
		img.width = 240;

		const res = await fetch(img.src)

		const base64img = res.url.split(";base64,").pop();

		let data = {usercardnumber, image: base64img};

		data = JSON.stringify(data)

		let xhr = new XMLHttpRequest();
		promisify(xhr);

		// open request
		xhr.open("POST",  `https://${DOMAIN}:${PORT}/api/login`);

		// set `Content-Type` header
		xhr.setRequestHeader("Content-Type", "application/json");

		// send request with json payload
		xhr.send(data).then(res => {
			// response: { meetingUrl: string,
			// meetingUser: string,
			// meetingPassword: string,
			// result: obj,
			// token: string}
			const response = JSON.parse(res.response);

			// Show user and password
			alert(`user: ${response.meetingUser}, password: ${response.meetingPassword}`);

			// Token was send in the url to use for the other server
			window.location.href = response.meetingUrl;
		}).catch(()=> {
			// Redirecto to a new url
			if(FAIL_LOGIN_REDIRECT_URL){
				window.location.href = FAIL_LOGIN_REDIRECT_URL;
			}
		});

		// stop Streaming
		await stopStreaming();
	}
}

// Enable login btn
function enableLoginBtn(usercardnumber){
	// Get htm elements
	const inputElement = document.getElementById("usercardnumber");
	const btnStartElement = document.getElementById("btn-start");
	const btnRegisterElement = document.getElementById("btn-register");
	const errorMsgElement = document.getElementById("error-msg");

	// Clear login error style
	clearStyle();

	// Get usercardnumber
	usercardnumber = ""+usercardnumber.trim();
	if (usercardnumber.length > 5) {
		// Remove disabledbtn class and enable again
		btnStartElement.className = btnStartElement.className.replace(/\bdisabledbtn\b/g, "");
		btnStartElement.disabled = false;
	}else{
		// Add disabledbtn class
		if(!btnStartElement.className.includes("disabledbtn")){
			btnStartElement.className = btnStartElement.className + " disabledbtn";
		}
		btnStartElement.disabled = true;
	}
}

// Change css and display login error
function errorStyleHandle({errorCode}){
	// Get html elements
	const inputElement = document.getElementById("usercardnumber");
	const btnStartElement = document.getElementById("btn-start");
	const btnRegisterElement = document.getElementById("btn-register");
	const errorMsgElement = document.getElementById("error-msg");

	// Set Error message
	errorMsgElement.innerHTML = msg[errorCode];

	// If error == FACE_IMG_NOT_FOUND, enabled to register de face
	if(errorCode  === "FACE_IMG_NOT_FOUND"){
		// hide login btn
		if(!btnStartElement.className.includes("displaynone")){
			btnStartElement.className = btnStartElement.className + " displaynone";
		}
		// Show register btn
		btnRegisterElement.className = btnRegisterElement.className.replace(/\bdisplaynone\b/g, "");
	}else{
		if(!inputElement.className.includes("userinputerror")){
			inputElement.className = inputElement.className + " userinputerror";
		}
	}
}

function clearStyle(){
	// Get htm elements
	const inputElement = document.getElementById("usercardnumber");
	const btnStartElement = document.getElementById("btn-start");
	const btnRegisterElement = document.getElementById("btn-register");
	const errorMsgElement = document.getElementById("error-msg");

	// Clear login error style
	errorMsgElement.innerHTML = "";
	inputElement.className = inputElement.className.replace(/\buserinputerror\b/g, "");
	if(!btnRegisterElement.className.includes("displaynone")){
		btnRegisterElement.className = btnRegisterElement.className + " displaynone";
	}
	btnStartElement.className = btnStartElement.className.replace(/\bdisplaynone\b/g, "");
}